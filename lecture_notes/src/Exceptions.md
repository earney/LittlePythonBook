## Exceptions

What is an Exception?

An exception is an event, which occurs during the execution of a program that disrupts the normal flow of the program.
Exceptions allow us to handle situations that are not expected, or are not ideal in an executing program.  In this lecture, we will look at some 
basic exceptions and how to handle them.  Exceptions can become an advanced topic, so we will try to just focus on what we need to know
(ie, the basics) in this course.

We have been introduced to the int and float functions.  These convert a string value into an integer or real value.

```python
>>>int("10")
10
```

But what happens if we pass a string that contains a value (ie, "abc" that cannot easily be converted into a number?

```python
int("abc")
```

produces a ValueError exception:

```python
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: invalid literal for int() with base 10: 'abc'
```

We can handle (or "catch") these errors by using a try statement:

### Try syntax:

```xml
try:
	<try body>
except:
	<except body>
```

We execute the code in the "try body".  If there is a problem (an exception is raised), we jump out of the "try body", and execute what is in the
except body.

Let us look at some examples:

We could rewrite the above statement like so:

```python
try:
	int("abc")
except:
	print("Error! 'abc' cannot be converted to an integer.")
```

When we execute this "Something went wrong!" gets displayed since the string "abc" cannot be converted into an integer.

Of course, the example above is quite limiting, so we could expand it by asking the user for a number:

```python
res = input("Please enter in an integer: ")

try:
	num = int(res)
except:
	print(f"Sorry!  {res} cannot be converted into an integer.")
```

The example above is a classic example, which you will see in a lot of python programs.  When you accept input from a user, you need to check that
the input data is acceptable.

So if the user enters in a string that can be converted into an integer value, the num variable will contain that integer, otherwise, the error message 
is displayed.


Here's an example, which will check raise an exception because we cannot divide by zero:

```python
denom = 0
numerator = 10

try:
	num=numerator/denom
except:
	print("Error, cannot divide by zero.")
```

Exceptions are expensive (ie, are cpu intensive, and require additional resources), so it would be better to write the above example like so:

```python
if denom == 0:
	print("Error, cannot divide by zero")
else:
	num=numerator/denom
```

The above try/except syntax catches all exceptions, but what if we only want to catch certain types of excepts?  Actually we can expand the try/except syntax
like so to catch certain exceptions:

```xml

try:
	<try body>
except <exception>:
	<except body1>
except:
	<except bodyN>
```

Let us look at the divide by zero example:

```python
try:
	num=numerator/denom
except ZeroDivisionError:
	print("Denominator is zero")
```

Let us look at another example:

```python
try:
	int(myvar)  # lets assume the variable myvar does not exist.
except:
	print("Value cannot be converted to an integer")
```

This gives us an inaccurate error, because the real issue is that myvar is undeclared (ie, not a variable, myvar does not exist).    If a string cannot
be converted to a integer, we have a ValueError Exception (as shown in the error message earlier).  

How do we fix this?  We can add an ValueError exception, so that our print statement is displayed only when an ValueError is thrown,
and another error message is displayed for other things:


```python
try:
	int(myvar)        # lets assume myvar is never set to a value.
except ValueError:
	print("Error! 'abc' cannot be converted to an integer.")
except:
	print("Something else went wrong...")
```

This example works, but what type of exception was thrown?  Our exception message "Something else went wrong" doesn't say much.

Here is the correct version:

```python
try:
	int(myvar)   # lets assume myvar is never set to a value
except ValueError:
	print("Error! 'abc' cannot be converted to an integer.")
except Exception as err:
	print(f"Something else went wrong... Exception:{err}")
```

will output:

```python
Something else went wrong... Exception:name 'myvar' is not defined
```

This is much better.  The keyword Exception is a catch all for all exceptions, and its information is stored in the err variable, which we can reference in
the body of the exception.  


Full list of builtin exceptions: [https://docs.python.org/3/library/exceptions.html](https://docs.python.org/3/library/exceptions.html)


## In class examples
1. Try out the example above dealing with converting a string to an integer value.  What input sequences are allowed.  Which ones are not?


## Want to learn more about Exceptions?

Here are some recommended sources, just in case you want to learn more about exceptions:

[Python Exceptions: The Ultimate Beginner's Guide](https://www.dataquest.io/blog/python-exceptions/)

[https://realpython.com/python-exceptions/](https://realpython.com/python-exceptions/)

[Python docs](https://docs.python.org/3/library/exceptions.html)


