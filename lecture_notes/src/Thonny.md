## Thonny

Thonny is a user friendly Python IDE (Integrated Development Environment) for code development.  

### Some facts about Thonny

Thonny is:
* user friendly
* an editor for Python code without unnecessary fluff (not a general purpose programming editor, ie, Visual Studio)
* written in Python itself and is in active development.
* free and open source.
* available for Windows, Linux and macOS.


Because it is pretty simple python editor, we will use thonny for class lectures, homework assignments, etc.  Feel free to use another editor if you wish, 
but my level of support may be lacking for other editors. So use other editors at your own risk.

See [thonny.org](https://thonny.org) for additional features and a demo on how to use thonny.  It also contains a FAQ, and WIKI on thonny.
