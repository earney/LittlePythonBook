## Functions

Functions allow us to break a problem into smaller problems that we can
solve one at a time.  Functions also allow us to easily reuse code.

We have already been exposed to some functions.  Some functions are 'built in' which means that python provides them for us:
For example, the input and print functions we introduced previously are built in functions.  For a list of built in function see
[https://docs.python.org/3/library/functions.html](https://docs.python.org/3/library/functions.html)

For a language to be useful, it must provide a way for users to define their own functions.  We call these **user defined functions**.

User Defined Function Syntax:

```xml
  def <function name> (<function arguments>):
        <function body>
```

The first line contains the required def statement follwed by the function name and then a list of  variables we will pass to the function.
The second line starts the function body, which contains one or more python statements, which are all indented the same amount.

Let us look at an example:

Many of the examples in this lecture are expecting a positive integer, so lets write a function that asks the user for a positive integer.
We will call this function 'get_positive_integer'.

```python
#########################################
# get_positive_integer
# message should be a string asking the use for input
# returns a positive integer value
#########################################
def get_positive_integer(message):
	value = input(message)
	while True:
		try:
			value = int(value)
			if value > = 0:
				return value     # return number since it is an integer and > 0
			else:
				print(f"Error!  '{value}' is not a positive integer ( > 0)")
				value=input(message)         #ask again, since our number is negative
		except:      # we cannot convert the input to an integer so ask user again...
			print(f"Error! Entered value '{value}' is not an integer")
			value=input(message)
```

Note that I have added a comment to the top of my function.  It is good programming practice to write a header comment
describing what your function does.  

Below is a function that calculates the summation of 1 to that integer

```python
###########################################
# summation(n)
# assuming n is a positive integer
# returns the sum of 1 to n
###########################################
def summation(n):
	sum = 1
	for i in range(n+1):
		sum = sum + i

	return sum
```

We then can call and execute the function by typing its name and providing any required arguments.

```python
result=summation(5)

print(f"The sum of 1 to 5 is {result}")
```

A few things to note here:
 * In this example, we are hardcoding the argument to the summation function, but the argument could also be a variable
 * The variable result contains the value returned from the function summation.


Now we will define a function that will calculate the factorial of a number
```python
#############################################
# factorial (n)
# assumes n is a positive integer
# accepts a positive integer and returns its factorial
#############################################
def factorial(n):
    result=1
	for i in range(2, n):
		result = result * i

	return result
```
 Now lets use the factorial function as well as the get_integer function to create a small
 program that calculates the factorial of a number.
 
```python
number = get_positive_integer("Please enter a positive whole number")
fact=factorial(number)
print("The factorial of {number} is %s" % fact)
```


Lets write a function that will convert a Fahrenheit temperature to Celsius and vice versa:

Just a reminder of the formulas needed:

Fahrenheit = Celsius x (9/5) + 32

Celsius = (Fahrenheit - 32) x 5/9

```python
##################################################
# Fahrenheit2Celsius(f)
# assumes an integer or a float that represents a fahrenheit temperture.
# returns a float that corresponds to the formula c = (f - 32) x 5/9
##################################################
def Fahrenheit2Celsius(f):
	return (f-32) * 5/9

##################################################
# Celsius2Fahrenheit(c):
# assumes an integer or a float that represents a celsius temperature
# returns a float that corresponds to the formula f = c x 9/5 + 32
##################################################
def Celsius2Fahrenheit(c):
	return c * 9/5 + 32

def str2float(s):
	try:
		f=float(s)
		return f
	except:
		print("Error invalid float value.. using zero instead")
		return 0
```

Now lets use these functions by asking the user for a temperature in Celsius and converting it to Fahrenheit:

```python
c = input("Please enter in a temperature in Celsius: ")
c=str2float(c)
print("%s degrees Celsius is equivalent to %s degrees Fahrenheit" % (c, Celsius2Fahrenheit(c)))

f = input("Please enter in a temperature in Fahrenheit: ")
f=str2float(f)
print("%s degrees Fahrenheit is equivalent to %s degrees Celsius" % (f, Fahrenheit2Celsius(f)))
```


 ### Variable scope

All variables have a limited life span.  Some variables live for the whole execution of a program, while others may only live in a function or group of functions.

To help clarify this, lets look at some examples.  In the factorial function we defined above, we have a variable called result.  Since the result variable is defined in the factorial function (with the result=1 statement), the
scope of result is the factorial function.  Once the factorial function stops executing the result variable does not exist anymore.  The value in result is returned (with the return result statement), but the variable itself is destroyed.

Lets look at another example:

```python
def complexCalc(a, b, c):
	value = a * a
	value = value + b*b
	value = value - c*c
	return value

x=input("Enter x")
y=input("Enter y")
z=input("Enter z")

# for simplicity, assuming user will enter in a correct value for x, y, and z..
result = complexCalc(float(x),float(y),float(z))
print(f"The answer is {result}")
```

Variables a, b, and c are defined in the function definition on the "header line" (line 1), so a, b, and c are local variables and 
constrained (only lives in) the complexCalc function.  When we call the function (line 11), we pass it three variables (x, y, and z), where the value of
x is passed to a, value of y is passed to b, and the value of z is passed to c.

 The variable value is defined in the function  (line 2), and only lives in the 
complexCalc function (lines 2-5).  Trying to access variables a,b,c or value outside the function will result in an error.
(**Give it a try!**  type in a, b, c, or value after line 6, and execute the code to see the errors produced).
 
 The result variable (defined on line 11), can only be used on lines 11, 12, or 13.  Any other place will produce an error.
 
 Lets look at another example:
 
```python
1.  x=5
2.  
3.  def doit(x):
4.   	x=x+1
5.  	return x
6.
7.  a=3
8.  y=doit(a)
9.
10. print(a, x, y)
```

We start off on line 1 with a varible (x), that is defined as containing an integer of value 5.
On line 3, we define a function called doit, with a variable argument named x.  On line 4 we add one to x and then on line 5 return the value of x
On line 7, we define a variable called a and assign it a value of 4.
On line 8, we define a variable called y, and assign it the value returned from the doit function when we pass the variable a.
On line 10, we print out the values of a, x, and y.

One thing to note.  The variable x on line 1, and the varible x on lines 3-5 are not the same x. The variable x on line 1 is of "global scope", which means that the variable
lives and can be accessed anywhere in the program.  The variable x on lines 3-5 are only accessable in the doit function, and will not alter the value
of the global x.

Now, type the program above into python and try different values for a and x, and see how the variables react.  You should  notice that the 
x defined on line 1 does not get altered, and will always output the value that is defined on line 1.   You can also modify the variable name in the 
doit function (for example change the name from x to x1 and the program still works the same, like I do below:).

```python
.
.
def doit(x1):
	x1=x1+1
	return x1
```

A good programming practice is to use different variable names so that the behavior is always clear.  For example, a better way of writing the program 
above would be:

```python
x=5

def doit(v):
	v=v+1
	return v

a=3
y=doit(a)

print(a, x, y)
```

As you can see, this program is a bit easier to read,  We know that x and v are not the same variable, and thus won't make the mistake of thinking that
the value of x defined in line1 is somehow modified later in the program.

### Functions that return values

As we have seen above functions can return values.  A function can return 0 or more values.  A return value is not required, but more than one is
possible if you need to return more than one value.

Lets look a function that returns more than one value

```python
1. def get_data():
2. 	return 1, 2
3.
4. first, second=get_data(data)
5.
6. print(f"The first return values is {first}")
7. print(f"The second return value is {second}")
```

Line 2 contains the return statement, and we return values 1 and 2. Line 4 calls the get_data function, and assigns variables first and second the values 1 and 2.


### Functions with Default values

Python allows us to provide default values for function arguments.  The default variables must come at the end of the argument list
We can easily do this by assigning a value to a variable in the argument list:

```xml
def function_name(var1, var2, var3,  varN=default value, varN1=default value):
```

```python
def calculateInterest(amount, interest=2.0):
	return amount + amount * interest/100.0

print("3 percent interest: %s" % calculateInterest(100, 3.0))
print("2 percent interest: %s" % calculateInterest(100))
```

We define a function called calculateInterest, which takes 2 arguments.  We start with our required variables (in this case, amount), and then list our
arguments (interest) that have default values (2.0 percent).

Let us look at another example where we ask the user for an integer value:

```python
def get_integer(message="Please enter an integer value: "):
	value = input(message)
	while True:
		try:
			value = int(value)
			return value
		except:
			print(f"Error! Entered value '{value}' is not an integer")
			value=input(message)

a = get_integer()
b = get_integer()

print("The sum of the two values is %s" % (a + b))
```

By setting message a "default value", we can just call get_integer if we are fine with the message that will be displayed.  If we need to change the
message we can do it like so:

```python
a=get_input(message="This will be displayed instead")
```

## Additional Exercises
1. Let us go back to the madlib exercise we had when we were introduced to Input/Output.  Create a function named get_word that will prompt a user 
for input.  If the user does not enter anything have the function randomly choose an item from a list which is provided as a function argument.  The 'get_word'
function should accept two arguments, the first being the message displayed asking for input.  The second argument should be the list of words that are used
if the user enters no input.

Here is the madlib:

The Declaration of Independence is a **\_\_adjective\_\_** document because it meant 
the **\_\_plural noun\_\_** of the **\_\_number\_\_** colonies decided to be
**\_\_adjective\_\_** from **\_\_country\_\_**.  In the year **\_\_number\_\_** the 
**\_\_adjective\_\_** Congress decided to **\_\_verb\_\_**  **\_\_number\_\_** leaders
 to write the Declaration of **\_\_adjective\_\_**.  They agreed to have **\_\_first name\_\_* 
 Jefferson **\_\_verb\_\_** the document and **\_\_verb\_\_** it to 
Congress after that.  After much debate, the leaders of each **\_\_noun\_\_** 
**\_\_verb\_\_** the Declaration of **\_\_adjective\_\_** on July **\_\_number\_\_**, 1776.

So to start your program you will do something like:

```python
adjective=get_word("Please enter an adjective: ", ["blue", "large", "attractive", "adventurious"])
plural_noun=get_word("Please enter in a plural noun: ", ["people", "buildings", "cars"])
.
.
.

print("fThe Declaration of Independence is a {adjective} document because it meant")
.
.
```

2. Write a function that returns True if a number is odd, and False if the number is even.  
Hint:  Use the modulo operator (%) to check for even/odd.  For example,  x%2 returns 0 if the x is even, and it returns 1 if x is odd.

3. Write a function called get_integer that returns an integer that was entered by the user. (look at the get_positive_integer function defined above, what 
changes will you need to make so that any integer (postive or negative) will be returned.

4. Write a program that asks the user for an integer, and prints out if the integer is even or odd.  Use the function you created in #2 above, as well
as the get_integer function we created in #3.

5. Update the get_positive_number function defined in the notes to only accept numbers within a range of values (ie, a minimum and a maximum value).

6. Write a function called generatePassword that accepts a number which represents the length of the password to be generated.  The function
should return a string of the length passed in, containing the following characters A-Z, a-z, 0-9, $, # @, _.

7. Extending on #6, make sure there is at least one number and one special character ($, #, @, etc) in the password, if not, you should generate another password.
 