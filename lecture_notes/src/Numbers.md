## Numbers
We all are used to using numbers in our math classes.  For a programming language to be useful, there must be arithmetic operators that can calculate things for us.  Python provides us with the necessary functions to perform calculations efficiently.  The two major number types that are provided are whole numbers (integers) and reals (floats).
Integers are whole numbers that do not contain a fractional component.  Numbers like 1, 2, 7, -4 are integers.  Python also supports real values or what python calls a floating-point real, or a float for short. Numbers such as 2.78, 3.14, 12.345, -5.0 are floats.
For every large integers, we can use an underscore (_) to separate individual digits so it is easier to read:

```python
>>> 10_000_000 + 54_321
10054321
```

In python, there is a limit to the precision of floats. A float value is only accurate up to 15 decimal places. After that, it rounds the number off.

```python
>>> 1.1111111111111111119
1.1111111111111112
```

Individual Exercise:  Type some positive and negative integers and floats into the python REPL to see how python interprets your input. 


## Arithmetic

In python we can do arithmetic with numbers to do some useful things.  Below are some basic operators:



| Operator |	Name	| Examples |
|-------------|------------|---------------|
| + | Addition	| 3 + 4 | a + b, a + 3.4, 4.5 + b 
| -	|Subtraction	| 4 – 3 | a – b, a – 3.4, 4.5 - b
| *	| Multiplication |	4 * 3 | a * b, a * 3.4, 4.5 * b
| / | Division	| 4/3 | a/b, a/3.4, 4.5/b
| % | Modulus	| 7 % 5 | a % b
| ** |	Exponent	| 4**2 | a**b, a**2, 4.5**b

Let us look at a few examples:

```python
>>> 3 + 4
7
```

Since both 3 and 4 are integers the result will be an integer.

Let us look at an example where one or more of the operands are floats:

```python
>>> 3.5 + 2
5.5
```

Since at least one of the numbers is a float, the result will also be a float.

For addition, subtraction, multiplication, and exponents, the resultant data type follows this same logic.

The exception to this rule, is division. Even if both operands are integers the result is always a float:

```python
>>> 4 / 2
2.0

>>> 4.0 / 2
2.0
```

### Time for some examples

Let us calculate the number of minutes in a year:

60 minutes in an hour
24 hours in a day
365 days in a year

So to calculate the number of minute in a year we multiply each of the numbers together:

```python
>>> 60 * 24 * 365
525600
```

What if we want to calculate the number of seconds in a year?
We can use the information from the last simple exercise with:
60 seconds per minute

```python
>>> 60 * 60 * 24 * 365
31536000
```

Lets look at another example.  The United States uses its own metrics to measure temperature, Fahrenheit, while most of the rest of the world uses
Celsius.  In this example, we will take a temperature in Fahrenheit and calculate the same temperature in Celsius:

The formula to convert Fahrenheit to Celcius is:

Celsius = (Fahrenheit - 32) x  9/5

```python
>>> temp_f =32
>>> temp_c = (temp_f - 32) * 9/5
>>> temp_c
0.0
```

We can also convert a celsius temperature to a fahrenheit temperature using the following formula:

Fahrenheit = Celsius X (9 / 5) + 32

```python
>>> c = 100
>>> f = c * 9/5 + 32
>>> f
212
```

### Additional Exercises:

1. Find the average of these test scores:  90 85 70 85 60

2. Multiplying by 6:   
	1. Take 6 and multiply by an even number.  
	2. The answer will end with the same digit:  
	3. For example 6 x **4** = 2**4**

3. The answer is 2:  
	1.	Take a number
	2.	Multiply it by 3
	3.	Add 6
	4.	Divide this number by 3
	5.	Subtract the number from step 1 from the answer is step 4.
	6.	The answer should be 2.

4. Same three digit number:  
	1.	Start with a 3 digit number where all the digits are the same (111, 222, 333, 444, etc)
	2.	Add up all the digits (111 => 3)
	3.	Divide the 3 digit number by the answer is step 2
	4.	The answer is 37.

5. Six digits become 3:
	1.	Take any 3 digit number and write it twice to make six digits (456 => 456456)
	2.	Divide the number by 7.
	3.	Divide it by 11
	4.	Divide it by 13
	5.	The answer is the original 3 digit number

6. 3 digits become 6:
	1.	Start with a 3 digit number
	2.	Multiply it by 7
	3.	Multiply it by 11
	4.	Multiply it by 13
	5.	Result will be a 6 digit number that repeats the number in step 1

7. Contains the digits 1, 2, 4, 5, 7, 8:
	1.	Select a number from 1 to 6
	2.	Multiply the number by 9
	3.	Multiply it by 111
	4.	Multiply it by 1001
	5.	Divide it by 7
	6.	The number will contain the digits 1,2,4,5,7,8.  (the number 6 produces 714285)

8. Create a variable named tip.  Assign a dollar amount to it (ie, $10.00).  Write the statements necessary to see what the tip should be if you tip 15%.