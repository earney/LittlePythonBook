# Table of Contents

[Table of Contents](Summary.md)

[Introduction](Intro.md)

[Thonny (python text editor)](Thonny.md)

[Numbers and Math](Numbers.md)

[Variables](Variables.md)

[Strings](Strings.md)

[Input/Output](input_output.md)

[Conditionals](Conditionals.md)

[If Statements](if_statements.md)

[Exceptions](Exceptions.md)

[Lists](Lists.md)

[Loops](Loops.md)

[Functions](Functions.md)

[Dictionaries](Dictionaries.md)

[Builtin Libaries](builtin_libraries.md)

[File IO](File_IO.md)

[Classes](Classes.md)

[todo](todo.md)