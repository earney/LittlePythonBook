## Dictionaries

Dictionaries allows us to store keys and value in a variable.

### Creating a dictionary

Dictionary Syntax:
```xml
<dictionary variable> = {key1: value1, key2:value2, ..  keyN: valueN}
```

key and values can be any type (strings, numbers (integers, floats), or any other type).

We don't have to specify keys or values.  So to create an empty dictionary we can do:

```python
myvar = {}
```

To create a dictionary with some intial values we can do:

```python
dict = {'name': "Billy", 'gender': 'Male'}
```

### Accessing elements of a dictionary

```python
dict = {'SSN': '1234', 'Name': 'Billy', 'Gender': 'Male'}

## lets access the SSN
print(dict['SSN'])      #prints out 1234
```

### Assigning a new Key/Value to a dictionary.

```python
variables = {}

variables["mykey"] = "myvalue"
variables['age'] = 12

print(variables)
```

outputs:
```xml
{'mykey': 'myvalue', 'age': 12}
```

### Modifying an existing key

Lets update the age key in the variables dictionary, to 13.

```xml
print(variables)
variables["age"] = 13
print(variables)
```

```xml
{'mykey': 'myvalue', 'age': 12}
{'mykey': 'myvalue', 'age': 13}
```

## How to remove elements in a dictionary

```python
d={}
d={"mykey": 123, "another key": "abc"}

del d["mykey"]

print(d)    # {"another key" : "abc"}
```

So far we have only seen keys of type string.  But keys of different types are legal in python as well.
Lets look at some examples where the key is a non string type:

```python
data = {1: 'one', 2: 'two', 3: 'three'}
print(data[1])
```

outputs:

```xml
one
```

### Retrieving each key/value in a dictionary (one at a time)


```python
variables.items()
```
outputs:

```xml
dict_values([('mykey': 'myvalue'), ('age', 13)])
```

We can use a for loop to look at each of the key/values, one at a time:

```python
for key, value in variables.items():
	print(key, value)
```

outputs:
```xml
mykey myvalue
age 13
```

### Retrieving a list of keys

At times, you may need to know all the keys that have been defined in a dictionary.  You can do that with the keys function:

```python
print(variables.keys())
```

outputs:
```xml
dict_keys(['mykey', 'age'])
```

We can use a for loop to look at each key one at a time:
```python
for i in variables.keys():
	print(i)
```

outputs:

```xml
mykey
age
```

### Retrieving a list of values
Just like keys, there might be times when you want to list the values in a dictionary:

```python
print(variables.values())
```
outputs:

```xml
dict_values(['myvalue', 13])
```

You can also use a for loop to look at each value:
```python
for v in variables.values():
	print(v)
```

outputs:

```xml
myvalue
13
```



## Additional Exercises

1. Write a program that asks the user for a list of numbers.  Calculate the mode from the list.  
Hint: The mode is the number that occurs the most frequent in the list.  Use a dictionary to keep
track of the number of times a word occurs.

2. Create a dictionary that contains the keys 'a' through 'z', and set the value for each key to be 2 letters further down the alphabet.  a=> c,
b=>d, ....  x=> z, y=> a, z=> b.   Ask the user for a statement/message, now use the dictionary you just created to scramble the message.  For each 'a'
in the message, you will translate it to a 'c', each 'b' will be translated to 'd', etc.

3. Continuing exercise #2 above, take a 'scrambled' message and translate it back.  Each 'c' should be translated as a 'a', etc.  You should use the same
dictionary you defined in #2 to complete this task.
