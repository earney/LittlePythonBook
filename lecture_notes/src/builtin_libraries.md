
## Builtin Libaries

So far all the functionality we have seen has been "built in" to the python interpreter by default.

It is almost impossible to include everything you need directly into the python interpreter itself.  Instead, we can use some builtin libraries to expand
its usefulness.

We need a way to tell python to "add" a library so we can access and use it.  Import is a python command that can be used to include a library.

```xml
import <library name>
```
where library name is a single library taken from [https://docs.python.org/3/library/index.html](https://docs.python.org/3/library/index.html)


How do we know what is available in these libraries?  We have a few options.  First, the official python docs is a great place to start.   If you want
a quick and dirty way to see if a function is avalable you can use the "dir" command.  The dir command will produce a list of all "objects" 
(ie, variables, functions, etc) that are available.
 [https://docs.python.org/3/library/functions.html?highlight=dir#dir](https://docs.python.org/3/library/functions.html?highlight=dir#dir)

```xml
dir(<library name>)
```

There are a hundred or so built in libraries, that are available for you to use.  But for right now we are only going to look at three libraries, random, sys and os.

### random

```python
import random
```

The random module (ie, library) contains several functions that allows us to choose an item from a list of items, or can generate a random number
in a range of numbers.

#### random.choice()

```xml
choice(<list of items>)
```
where 'list of items' is defined as [item1, item2, item3]

lets look at a few examples:

Several years ago, I had a friend that enjoyed going to lunch with co-workers.   Somedays it was hard to figure out which restaurant to go to.  
A colleague of ours decided write a program that randomly chosen a restaurant from a list.  We can emulate this with the following python statement:

```python
>>> import random
>>> random.choice(["Wendy's", "Jacks'", "Arbys", "Olive Garden", "Taco Bell"]) 
Jacks
```

The list of items can be anything.. Here's a statement that prints out a random prime number:

```python
>>> import random
>>> random.choice([1, 3, 5, 7, 11, 13, 17, 19, 23])
17
```

randint() is a great function to use if you want a random number in from a range of integers:

```xml
randint(start, end)
```
Will pick a random number between start and end (includes start, and end).


Lets say we want to want to get a random number between 1 and 5:

```python
>>> import random
>>> random.randint(1,5)
2
```

Want to learn more about the [random module](https://docs.python.org/3/library/random.html?highlight=random#module-random)?

### sys

The sys library provides access to some variables used or maintained by the interpreter and to functions that interact strongly with the interpreter. 
It is always available.

You can access the function provided in the sys library by using the import command:

```python
import sys
```

There are many important variables in the sys module, but a variable you may see quite often if you inspect python code is the "version" variable.
Python is an active and evolving language.  Features can be added, removed, or changed from different verisons of python.  This sounds
more dramatic that it is in practice, but there are times when you want to know what version of python is executing our code: 

Let see what version of python I was using when I wrote the first rough draft of this lesson:

```python
>>> import sys
>>> sys.version
'3.10.6 (tags/v3.10.6:9c7b4bd, Aug  1 2022, 21:53:49) [MSC v.1932 64 bit (AMD64)]'
```

Another important variable in the sys module is the platform variable:

```python
>>>import sys
>>>sys.platform
'win32'
```

The platform variable will tell us which operating system we are currently on.  The platform variable can come in quite handy when we want
to write python code that will work on different platforms (Windows, macOS, Linux, etc).


Sys documentation: [https://docs.python.org/3/library/sys.html](https://docs.python.org/3/library/sys.html)

### os

The os library provides a portable way of using operating system dependent functionality.

os documentation: [https://docs.python.org/3/library/os.html?highlight=os#module-os](https://docs.python.org/3/library/os.html?highlight=os#module-os)


#### os.remove

os.remove will delete an existing file, if the file does not exist, a FileNotFoundError is produced.

```xml
os.remove(<file name>)
```

where filename is the name of the file you want to delete.  If the file is not in the current working directory, you must specify a path.

Here is an example:

```python
import os
os.remove("myfile.csv")
```

Later, when we talk about Files, we will use the os.remove function to remove a file if it already exists.


## Additional Exercises
1. Use the dir command to see a list of the members in the os, random and sys modules.
2. Can you use the random library to generate a new password?  How would you go about this using the information we have learned so far?
3. Study the online python documentation for the os and sys libraries (ie, modules).
4. A list of variables and functions in the sys module that are commonly used are sys.executable, sys.exit(), sys.implmentation, sys.version_info, sys.modules, sys.path. What do each of these do?
5. A list of variables and functions in the os module that are commonly used are os.name, os.environ, os.uname, os.chdir(), os.getcwd().  What do each of these do?

5. Try some of the commonly used functions and variables and see what happens in your environment.