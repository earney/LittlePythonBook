# If Statements

So far the programs we have discussed have a sequential flow.  In other words, each statement has executed once, starting at the top, and executing each statement one by one until we have reached the end.
If statements, allow us to branch (or fork), left or right depending on if certain conditions are true or false.  If statements allow us to execute code based on weather certain conditions are true or false.  

### If syntax:

```html
if  <condition>:
    <body1>    # execute when condition is true
else:
    <body2>   # execute when condition is false
```

The first line starts with 'if' followed by an expression that evaluates to true or false.
The second line is the if statement's body, which contains one or more python statements which
are indented the same amount.

The else portion of the syntax is optional.  Let us look at an example where this is the case:

```python
if age >= 18:
	print("You are eligible to vote") 
```

### if else

You can add an else clause if you want code to be executed when the condition in the if statement (age > 18) evaluates to false:

```python
if age >= 18:
	print("You are eligible to vote")    #executed when condition is true
else:
	print("You are not eligible to vote")    #executed when condition is false
```

### If else syntax:

```xml
if <condition1>:
     <condition1 is true body>
elif <condition2>:
    <condition2 is true body>
.
.
elif <condition n>:
     <condition n is true body>
else:
    <else body>
```


Here's an example where we convert a score to a letter grade, using the common 90 is an A, 80 is a B, etc.

```python
if score >= 90:
	grade = 'A'
elif score >=80:
	grade = 'B'
elif score >= 70:
	grade = 'C'
elif score >=60:
	grade = 'D'
else:
	grade = 'F'
```

Here is an example of how to check to user input is an number.

isnumeric is a string function that checks that all values in the string are values 0-9.

```python
age = input("Please enter your age")

if age.isnumeric():
   print("You entered a valid age")
else:
   print("You did not enter a whole number")
```
If a user inputs a few spaces before or after the number ('  19  ', for example) the isnumeric function will return False.  To eliminate this possibility, 
we could add age = age.strip() before the if statement to remove all leading and trailing spaces.

isalpha is a string function that checks to see if all positions in the string contains letters a-z.

```python
name = input("Please enter your first name")
if name.isalpha():
   print("Your name is: %s" % name)
else:
  print("Invalid name.  Your name can only contains values a-z")

response = input("please enter in a phrase")
if response.index("is") > -1:
   print("Your input contains the word 'is'")
else:
   print("Your input does not contain the word 'is'")
```

## Match case statement

Remember that python is constantly envolving.  As recent as the 3.10 version release, a new statement has been made available, called the match case statement.

Basic Match case syntax:

```xml
  match <variable>:
  	case <value 1>:
		< case 1 body>
	case <value2>:
		<case 2 body>
	.
	.
	case <value n>:
		<case n body>
	case _:
		<case default body>
```

```python
ans = "Y"

match ans:
	case "Y":
		print("Yes!")
	case "N":
		print("No!")
	case _:
		print("maybe?")
```

If we have more than one case that we should see as equivalent to each other, we can use the pipe symbol "|" to "OR" values
together:

```python
ans = "Y"

match ans:
	case "Y" | "y":
		print("Yes!")
	case "N" | "n":
		print("No!")
	case _:
		print("maybe?")
```
This example allows the variable ans to be "Y" or "y" to produce "Yes!", and "N" or "n" to produce "No!", and everything else
to print out "maybe?". 


### Additional Exercises
1.  You work at a cash register, where you need to give back change.  Write code that will take a value between 0, and 99 and will return the
number of quarters, dimes, nickels and pennies to give back.  Write your code so that the minimum number of coins are returned.   For example, if 25
cents are requested, you should return 1 quarter, not 2 dimes and a nickel, not 5 nickels, etc.

2. Your boss is so excited about the code you wrote for problem #1, that he wants you to write some code to exchange dollars in denominations of $1, $5,
$10, and $20 dollars using the minimum number of bills necessary.  Assume the dollar amounts will not be larger than $99.

3.  Can you combine #1 and #2 that will take core of dollars and cents?

4.  How would you modify #1 and #2 if a certain coin or dollar denomination was not available?  Ie, lets say quarters are not available, how would you modify your
code to handle such a case?

5. You are asked to write code to determine if a given year is a leap year or not.  Use the following logic to determine if a year is a leap year or not:
a. If a year is evenly divisible by 400 it is a leap year.
b. Otherwise, years evenly divisible by 100, are not a leap year
c. Otherwise, years divisible by 4 are leap years.
d. Every other year is not a leap year

Ask the user for a year, use the logic above to figure out if it is a leap year, and then output if the year is a leap year or not.

6. Write code to check if a date is a valid date.  Ask the user for a month (1-12), day (1-31) and a 4 digit year.  Here are 
some rules to follow:

a. A month should contain a value between 1 and 12
b. If a month is 4,6, 9 or 11, the maximum number of days is 30.
c. In Feburary (ie, month = 2), we will assume that the maximum number of days is 28.
d. All other months contain 31 days.

7.We all know that Feburary contains 29 days during a leap year.  Update your code in number 6 above so that we incorporate
a leap year into the logic.  You will need to use the logic from #5 above to get this working correctly.

8. We want to ask the user for a phrase and check if the phrase is a palindrome.  
A Palindrome is a word, phrase, verse, or sentence that reads the  same backwards or forwards.
For example: "A man, a plan, a canal, a Panama!".    

We can use the following code to reverse the order of letters in a string:
```python
string = "".join(reversed(string))
```