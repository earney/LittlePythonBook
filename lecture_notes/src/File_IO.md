## Reading and Writing to Files

Reading input data from the keyboard is fine when we have a limited amount of data, say 2 or 3 inputs.  There are cases where you need to input
several dozen, or maybe hundreds of pieces of data.  Reading and writing from/to files allows us an easy way to accomplish this task.

### Opening a file

To read or write to a file, you must first open it. We use the open function to to state what file we want to access, and how we want to access it.

```xml
<file pointer> = open(<file name>, <access codes>)
```

where 
file pointer is a variable that we will use to read/write, and close the file when we are done.
filename is a string name such as "mydata.txt"
access codes states how we  want to open the file ("r" for read, "w" for write)


#### Opening a file for reading

Here we open a file named "myfile.data" for reading.   The location of the myfile.dat is the "current working directory", which is usualy the same directory
where the python script is that you are executing.

```python
fp = open("myfile.dat", "r")
```

I like to keep my data separate from my scripts.  In small programs, this is not neccessary, but there have been times when you may have to process
several data files, and having them in their own directory, just makes things a bit cleaner.  We can do this by writing:

```python
fp = open("data/myfile.dat", "r")
```

The python statement above will try to look for a directory called "data" in the current working directory, and then try to access the myfile.dat file in that
directory.

What happens if the "myfile.dat" file cannot be found?  A FileNotFoundError error is raised and displayed.

#### In class exercise

1.  Try the open statements above.  When you execute that code, does it produce a FileNotFoundError?  If so, why?


#### Open a file for writing

Quite often, you will want to write data out to a file instead of, or as well as displaying some info to the screen..  Sometimes it is summary data, or
maybe you need to change the format or data in some way.

Before we get ahead of ourselves, lets look at an open statement, that will allow us to write data to a file.

```python
fp = open("myoutput.txt", "w")
```

In the example above, a new file is created that we can write to.

What if the filename already exists?  The previous contents of the file will be erased.

### Reading from a file

```xml
<variable> = <file pointer>.read(<size>)
```

where size is an optional integer.  If we do not specify size, we read all of the file, and put the contents of the file in "variable".

```python
fp = open("mydata.dat", "r")
data = fp.read()
fp.close()

## do stuff with data here..
```


### Reading a single line from a file

readline() reads a full line of data from a file.

readline syntax:
```xml
<variable> = <file pointer>.readline(<size>)
```
where size is an optional integer value,

Here's an example:

```python
fp = open("students.txt", "r")
line=fp.readline()
while line:
	#do stuff with line data here...
	
	# read another line to check for end of file (EOF)
	line=fp.readline()
	
fp.close()
```

### Writing to a file

To write some data to a file, we use the write function.

The open function second argument should be "w".  To add to an existing file, you can use "a" to append to the file.

```xml
<file pointer>.write(<variable>)
```
where variable is a string you want to write to the file.

```python
f = open("myoutput.txt", "w")
f.write("This is my data!!")
f.close()
```


### Closing a file

Once we are done reading/writing to a file, we need to close it to guarantee no data is lost.

Close Syntax:

```xml
<file pointer>.close()
```


```python
fp = open("myfilename.dat", "r")

# perform file operations here...

fp.close()
```

### Some examples...

First, lets write to a file, and then later we will read from the same file..

```python

# lets create a file and write the fibonacci sequence to the file
myfile = open("fib.txt", "w")

# lets calculate the fibonacci sequence (for all values less than 100) 
# and print out the results
a=0
b=1
while a+ b < 100:
	myfile.write(f"{a}\n")
	temp = a + b
	a = b
	b = temp

myfile.write("%s\n" % a)
myfile.close()

## we should now have a file named "fib.txt" that contains one number per line

## now lets read the file all at once and print it out.
print("fib.txt  (with a single read)")
f = open("fib.txt", "r")
data = f.read()
f.close()
print(data)

## now lets read the data one line at a time..
print("fib.txt (with several readlines())")
f = open("fib.txt", "r")

line = f.readline()
while line:
	print(line)
	line = f.readline()
	
f.close()
```

A few things to point out.  The last bit of the output is double spaced.  Why is this?  The end of line (EOL) character "\n" is being printed out twice.  
The first "\n" is from the input read with the readline statement.  The second "\n" is produced when we use the print statement.  The print statement's
default action is to print out the the contents in the print statement, and to end with line with a "\n".

So how do we fix this?  The simpliest way is to modify how we use the print statement.

First I have to confess something that has been heavy on my chest.  I have been lying to you...   The official syntax of the print statement is:

```xml
print(*objects, sep=' ', end='\n', file=sys.stdout, flush=False)
```
[https://docs.python.org/3/library/functions.html#print](https://docs.python.org/3/library/functions.html#print)

Looking at the print statement syntax, we can modify the default action of ending a print with a "\n" by doing the following:

```python
print("I feel so much better now", end="")
```
We replace end="\n" with end="" (or some other value), so that we do not terminate the line.  So now, the "\n" at the end of the readline statement
will be the character sequence that will terminate the line:

```python
print("fib.txt (with several readlines())")
f = open("fib.txt", "r")

line = f.readline()
while line:
	print(line, end="")
	line = f.readline()

f.close()
```

### Write code that reads "settings" from a file and puts the values into a dictionary.

Lets look at an example where we read settings from a file, and put them into a dictionary.  Each line of the file contains a variable and a value:

Here's an example of how the settings file may look like:

```xml
width: 640
height: 480
timezone: central
```

Lets write some code to read this file, and put the contents into a dictionary called settings:

```python
settings={}

fp = open("settings.txt", "r")
for line in fp:
	key, value=line.split(':')
	# lets remove the newline character ('\n') 
	# and any white space (ie, space characters)
	# that might be in the value
	value = value.strip()
	settings[key]=value

fp.close()

## lets print out the setting dictionary
print(settings)
```


#### Additional Exercise

1. In the last example, trying changing the "" of the `end=""` of the print function  to some other sequence of characters and rerun the code to see what happens.

2. Sometimes we are given data in a format that might be difficuilt for a program to read.  In this example, your PE teacher is asking you to help him out 
on a task he needs to complete.  He has a handwritten list of BMI (body mass index) scores.  He wants you to write program to calculate the minimum, 
maximum, and average BMI score for the football team.  The BMI values are: 20, 32, 28, 40, 22, 31, 18, 26, 28, 24.

For this assignment, you will need to enter the scores, one per line to a file named bmi.txt.  Then you will need to write a program to read the bmi.txt
file and output the minimum BMI, the maximum BMI and the average BMI values.
