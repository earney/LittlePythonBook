## Strings

In a nutshell, a python string is just text that starts and ends with a single(') or double quote(")
"hello" is the same as 'hello'.   The quotes must match (ie, be the same).  If you start a string with a double quote, you must end it with a double quote.

Something like
```python
"hello'
```

 will produce a Syntax error (notice the string starts with a double quote, but it ends with a single quote).
```python
SyntaxError: unterminated string literal (detected at line 1)
```

You can create a variable that contains a string.

```python
>>> a= "hello"
>>> a
"hello"
```

I usually like to use double quotes for strings. That way things like:

```python
>>> "Billy's car"
"Billy's car"
```

is easy to produce.

You can add one string onto the end of another (string concatenation) by using the + operator:
```python
>>> a= "Hello " + " World"
>>> a
"Hello World"
```

What if we want to add a number to a string?
```python
>>> num = 146
>>> "CIS  "  + num
```

This will produce an error, because we don't know how to add a number to a string, but we can do the following instead:
```python
>>> number = 42
>>> "The number you entered is %s" % number
"The number you entered is 42"
```

### Strings vs Numbers

Most programming languages see strings and numbers as different things.  For example the string "1" is not equal to the integer 1 or float 1.0.
We can examine these differences by looking at a few examples:

```python
>>> 1 + 1
2

>>> "1" + "1"
'11'

>>> "1" + 1
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: can only concatenate str (not "int") to str

>>> 1 + "1"
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for +: 'int' and 'str'
```

To confuse matters, multiplication of a string and a number does work:

```python
>>> "=" * 3
'==='
```

### int(), float() and str()
So what do we do if we need to convert a string to a number or vice versa?  Python provides the str(), int(), and float() functions for this very purpose.

To convert a number to a string:

```python
>>> str(1)
'1'

>>> str(3.14)
'3.14'
```

To convert a string to an integer:
```python
>>> int("1")
1
>>> float("3.14")
3.14
```

The int and float functions are a bit flexible and do allow some special non digit characters (such as the tab key, and spaces) at the beginning
or ending of string:

```python
>>> int("      123        ")
123
>>> int("  456")
456
>>> int("789    ")
789
```

But int and float does not tolerate spaces or tabs in the middle of the string:

```python
>>> int("  1    0   ")
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: invalid literal for int() with base 10: '  1   0   '
```


In the example above, we converted the string "3.14" to a float, but what happens if we used the int function instead?  We just receive the integer
portion of "3.14".

```python
>>> int("3.14")
3
```


### Common String Functions


Please note that 1 is not the same as '1'.   1 is an integer (a number), while '1' is a string (ie, text).  Python as well as most other programming 
languages see these as two different things.


#### count

Return the number of non-overlapping occurrences of substring sub in the range [start, end]. Optional arguments start and end are interpreted as in slice notation.

count syntax:
```xml
str.count(sub[, start[, end]])
```
Examples:
```python
>>> "Mississippi".count("is")
2
>>> "Mississippi".count("is", 3)
1        (since we ignore the first 3 characters).
```

#### find
Return the lowest index in the string where substring sub is found within the slice s[start:end]. Optional arguments start and end are interpreted as in slice notation. Return -1 if sub is not found.

find syntax: 

```xml
str.find(sub[, start[, end]])
```

Examples

```python
>>> "Mississippi".find("is")
1
```

#### format
Format returns a string that replaces placeholders in the string with the values in the argument list 

format syntax:

```xml
str.format(values)
```
Examples

```python
>>>"I love CIS {}!".format(202)
"I love CIS 202!"

>>>instructor="Billy"
>>>"My CIS 202 instructor is {}".format(instructor)
"My CIS 202 instructor is Billy"
```

we can also use more than one variable/values at a time:

```python
>>> "I am enrolled in {}, and my instructor is Mr. {}".format("CIS 202", "Earney")
'I am enrolled in CIS 202, and my instructor is Mr. Earney'
```


#### Length of a string:

Return the length of a string s.

len syntax:
```xml
len(s)
```


```python
a="this is my text"
len(a)
15
```


#### lower

Return a copy of the string with all the cased characters are converted to lowercase.

lower syntax: 
```xml
str.lower()
```

```python
>>> "ABC".lower())
'abc'

>>> "aBc".lower()
'abc'
```

#### lstrip

Return a copy of the string with leading characters removed. The chars argument is a string specifying the set of characters to be removed. If omitted or None, the chars argument defaults to removing whitespace. The chars argument is not a prefix; rather, all combinations of its values are stripped:


lstrip syntax:
```xml
str.lstrip([chars])
```

```python
>>> "   abc   ".lstrip()
"abc   "

>>> "civic".lstrip("ic")
"vic"

>>> "radar".lstrip("ar")
"dar"

>>> "this is a string".lstrip("th")
"is is a string"
```

#### replace
Return a copy of the string with all occurrences of substring old replaced by new. If the optional argument count is given, only the first count occurrences are replaced.

replace syntax: 
```xml
str.replace(old, new[, count])
```
examples:
```python
>>> "Hello World!".replace("World", "Billy")
"Hello Billy!"

>>> "this is another test".replace("another", "the")
"this is the test"
```


#### rstrip
syntax: str.rstrip([chars])

Return a copy of the string with trailing characters removed. The chars argument is a string specifying the set of characters to be removed. If omitted or None, the chars argument defaults to removing whitespace. The chars argument is not a suffix; rather, all combinations of its values are stripped:
```python
>>> "   abc   ".rstrip()
"   abc"

>>> "civic".rstrip("ic")
"civ"

>>> "radar".rstrip("ar")
"rad"
```

#### strip
syntax: str.strip([chars])

Return a copy of the string with the leading and trailing characters removed. The chars argument is a string specifying the set of characters to be removed. If omitted or None, the chars argument defaults to removing whitespace. The chars argument is not a prefix or suffix; rather, all combinations of its values are stripped

```python
>>> "   abc  ".strip()
"abc"
>>> "civic".strip("ic")
"v"
>>> "radar".strip("r")
"ada"
>>> "this is a string".strip("th")
"is is a string"
```

#### upper
syntax: str.upper()

Return a copy of the string with all the cased characters are converted to uppercase. 

```python
>>> "xyz".upper()
'XYZ'
>>> "hello".upper()
'HELLO'
```

## Formatting Output

There are times when you want to customize how output is displayed.

For example, lets say you have a variable pi, and you want to display it with 3 decimal places.

```python
>>> "pi is %4.3f" % 3.1415926
'pi is 3.142'
```
or we can use the format function to complete the same task:
```python
>>> "pi is {:4.3f}".format(3.1415926)
'pi is 3.142'
```

### format specifiers

| **Value** | **Presentation Type**      |
|--------------|-------------------------------------|
| b              | binary integer                         |
| c              | single character                      |
| d             | Decimal integer                      |
| e or E     | Exponential                            |
| f or F      | Floating point                          |
| g or G    | Floating point or Exponential |
| o            | Octal integer                          |
| x or X     | Hexadecimal integer             |
| %           | Percentage format                 |

```python
>>> '%d' % 42
'42'
>>> '{:d}'.format(42)
'42'

>>> '%f' % 2.78
'2.780000'
>>> '{:f}'.format(2.78)
'2.780000'

>>> '%x' % 30
'1e'
>>> '{:x}'.format(30)
'1e'
```

####f-strings
In python 3.6 f-strings were introduced.  f-strings allow an easier way to provide string formatting:

```python
name = "Billy"
course = "CIS 202"

>>> f"Hello, my name is {name}, and I teach {course}"
"Hello, my name is Billy and I teach CIS 202"
```

You can also do some basic arthimetic in f-strings:

```python
>>> f"{2*21}"
'42'
```


### Additional Exercises:
1. Write the python statement(s) necessary to replace the word "men" with "people" in the statement "All men are created equal.".

2. Create a variable named title that contains the string "Python is hard to learn!".  Then perform the following tasks:
	1.	Find the length of title.
	2.	Replace the word 'hard' with the word 'fun' in the variable title.
	3.	Use the find string function to locate where the word 'is' is located in the variable title.
	4.	Modify your title variable so that it contains the uppercase version of title. 

3. Write the python statement(s) to convert 3.14159 to a string.

4. Write the python statement(s) necessary to create a string variable, assign it a string value, and then remove all leading and remaining white space.

5. Write the python statement(s) necessary to combine the following variables into a new variable called message (so that message contains "the secret number is 42"):
	text = "the secret number is "
	Number = 42
	
6. Writing in all uppercase is ALOT LIKE YELLING!.  Create a variable named message that contains a mixture of lower and upper case letters.
    a. emulate yelling by displaying all the letters in uppercase.
	b. now emulate whispering by displaying the message in all lowercase.
	
7.  You have a keyboard with a broken O (oh) key.  You get around this by typing a zero (0) .  You want to write a command that will take a string
and convert all zeros (0) to O (oh).


### Python String Documentation

String functions: [https://docs.python.org/3/library/stdtypes.html#string-methods](https://docs.python.org/3/library/stdtypes.html#string-methods)