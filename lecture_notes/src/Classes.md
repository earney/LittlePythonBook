## Classes

One of python's greatest assets is that it is object oriented, meaning that everything can be seen as an object.  It is a way to "black boxing" a 
problem and solution so that we only need to be concerned about certain issues/problems at any given time.

A popular way to solve problems is to take a larger problem and break it down into smaller pieces, until we get to pieces small enough that they
are reasonably easy to solve.  Object oriented programming is one way of taking a larger problem and solving smaller individual problems.  Once all
the smaller problems are solved, we can use those pieces to solve a bigger problem.

You have been using Object oriented programming already in this class, (see strings, integers, floats, lists, dictionaries).  These are all objects.  
Below we will learn how to create our own objects to solve problems we want to solve.

The basic class definition is below.

### Basic Class definition

```xml
class <class name>:
	def __init__(self, <arguments>):
		<body>
		
	def <function name1> (self, <arguments>):
		<body>
	.
	.
	def <function namen>(self, <arguments>):
		<body>
```

The \_\_init\_\_ function is where we set variables to initial values, etc.  The other functions we will need to define depend on what the class will do, 
so they can do just about anything.  This gives a lot of flexibility.

Let us create a class named Person that keeps track of the number of laps someone walks:

```python
class Person:
	def __init__(self, name, age):
		self.name=name
		self.age=age
	
		self.laps=0      # keep track of the number of laps someone walks

	def walk(self):
		self.laps+=1      # same as self.laps = self.laps + 1

	def get_laps(self):
		return self.laps
		
	def __str__(self):   # used when we want a string representation
                                  # of this person  (used in print, etc)
		return "name:%s, laps:%s" % (self. name, self.laps)
```

We have defined the person class,  In the \_\_init\_\_ function, we set the name of the, person, as well as their age, and we initialize a laps variable to zero
since we will be using this variable to keep track of the number of laps someone walks.  The walk function, increments (or adds one) to the laps variable.
The get_laps function returns the number of laps someone has walked.

The \_\_str\_\_ function defined above is a special function that is used to represent the Person object as a string.   When you print out a person, or use an str function
on a person object, the \_\_str\_\_ function is called.


Let us use the person class in a program:

```python
myperson = Person("Timmy", 20)

## have timmy take a lap
myperson.walk()

## time for timmy to take another lap
myperson.walk()

print(myperson)
```

This should produce:
```python
name:Timmy, laps:2
```

There is a lot of new stuff going on here, so let me explain:

```python
myperson = Person("Timmy", 20)
```

The statement above creates a Person class variable named myperson.   We set myperson's name variable to "Timmy", and its age variable to 20.
myperson is an instance of the class Person.  Think of the class as the "blue print of a house", and the variable (myperson) as the 
instance (or physically built house).

```python
myperson.walk()
myperson.walk()
```

todo: maybe create a graphic showing the myperson instance with it variables getting updated (ie square with vars in it, etc)

myperson.walk calls the myperson's walk function and adds one to its laps variable, and then the second call, adds one more to laps.

```python
print(myperson)
```

Yes, this is not a typo, you can print an [object](https://docs.python.org/3/library/functions.html#print).
We define what is printed with the \_\_str\_\_ function we defined in the Person class.
In the \_\_str\_\_ function, we return the string we want to represent the class.

What happens if we remove the \_\_str\_\_ function (ie, no \_\_str\_\_ function defined)?

Python does the best it can, and prints out something like:
```xml
<__main__.Person object at 0x000002A8AAFEF730>
```

This means that it is a Person object, with no \_\_str\_\_ function defined.  the weird numbers/letters at the end represents the memory location of the
person object.  There usually isn't a need to know that memory location, but if you want to compare if two objects are the same, you could compare
the memory locations to see if they are the equivalent.  It is beyond the scope of this class, but there is a function that allows us to compare if two objects are the same.
It is called \_\_eq\_\_.


Now lets look at an example where we have two Person objects:

```python
john = Person("John", 30)
sally = Person("Sally", 25)

sally.walk()
john.walk()
sally.walk()

print("print out each status")
print(sally)
print(john)
```

```python
name:Sally, laps:2
name:John, laps:1
```

Some things to note.  I referred to a black box earlier.  When we call the Person class, we don't have to be concerned about how things
are working internally, (if we don't want to).  We can just use the class, and it should work as it should.  This allows us to write complex 
problems, by breaking the larger problem into smaller problems, and solving those smaller problems first, and then build a solution for our bigger problem.


A few minutes ago, we talked about the \_\_str\_\_ function.  Python provides a bunch of methods with these "special names". \_\_add\_\_ is another.
We can use this function to define how two objects behave when we try to add them together.

```python

class Worker:
   def __init__(self, id, name, age, wage, num_dependents, number_of_years):
      self.id = id
	  self.name=name
	  self.wage=wage
	  self.num_deps = num_dependents    #number of dependents
	  self.num_years = number_of_years   #number of years working here
	  
   def __str__(self):
   	    return "Name: %s " % self.name

   def __add__(self, other):
       return self.wage + other.wage


Jolon = Worker(123, "Jolon", 30, 15.00, 2, 5)
Becca = Worker(456, "Becca", 32, 16.00, 3, 7)

print("Total wages of Jolon and Becca are: %s" % (Jolon + Becca))
```

In the \_\_add\_\_ function, self refers to itself (ie, its own object, the Worker object on the left side of the plus sign), 
while the "other" variable refers to the instance of the Worker on the right side of the plus sign.

What would we need to change so that we got a total of dependents instead of wages?

The \_\_add\_\_ function would need to look like this:

```python
def __add__(self, other):
	return self.num_deps + other.num_deps
```

For a full list of "special name" operators see: [Special method names](https://docs.python.org/3/reference/datamodel.html?highlight=__str__#object.__new__)

### Going the extra mile...
Can we add other things besides just instances of the same class?  (in this example, it was two workers).  Could we add an other object, or even
an integer value.   Yes we can!  Do do this, we need a built in function called isinstance that checks to see what class the "other" variable is.

```xml
	isinstance(obj, type)
```
where obj is the object we want to check to see what "type" (or class) it is
and type is the name of the object we are checking against.


```python
def __add__(self, other):
	if isinstance(other, int) or isinstance(other, float):
		return self.num_deps + other
	elif isinstance(other, Worker):
		return self.num_deps + other.num_deps
	else:
		### should produce an error here, since we don't know to deal with this..
		pass  #for now we will just pass (or ignore errors)
```


#### Additional Exercises

1. Update the Person class so that it contains a run function that updates the laps variable by 2 each time the run function is called.
(So if someone runs a lap, it is equivalent to walking two laps.)

2. Update the Person class so that it contains a new variable called laps_goal, that defines the number of laps a user should walk daily.
Update the Person class so that when a new Person class is created, laps_goal is defined as a specific number.
Create a function called goal_met that returns True when a person mets or exceeeds laps_goal, and False otherwise.
Call the goal_met function and print out "Goal Met!" or "Goal not Met!"

3. Create a new class called student that contains name, student number, gpa, gender, and race.  Create an instance of the student class using your
 name, your student number, your race, and a gpa of 3.5. Create functions for the student class that allows you to modify the gpa.

4.  Retention has become a big issue at your place of work.  To address this issue, your boss wants to see how much it would cost to give raises based
 on the number of years someone has been with the company.  Create an Employee class, which contains a name, salary, and number of years
  employed as variables.  Write a class function named "pay" that calculates the total pay after the raise is given to an individual., 
Base the raise on values in the following table:

| number of years | percent raise |
|-----------------------|-------------------|
| 0 - 3                   |        4 %         |
| 4 - 6                   |        8 %         |
| 7+                      |       10 %        |

Create at least one instance for each category in the table to fully test your code.
