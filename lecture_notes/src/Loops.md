## Loops

So far we have talked about executing code that is sequential, meaning that we start at the first line, and execute each line one at a time until
we reach the end.  We then talked about executing code based on if a condition is true or false (control flow).  This allows our programs to make decisions based
on variable values and conditions.

We will now talk about another concept that allows us to execute the same piece of code several times based on conditions.

Loops allow us to execute the same piece of code several times.  In python there are two main types of loops, for loops and while loops.

### For loops

```xml
for <variable> in <list of items>:
	<for body>
```

![for](images/for-loops-in-Python-300x300.webp)

Lets say we have a list of items, and we want to print out each item on its own line:

```python
words = ['Python', 'is', 'the', 'best!']
for word in words:
	print(word)
```


Quite often we want to perform a task a certain number of times (ie, counting),  we could produce a list like so:
```python
mylist = [1, 2, 3, 4]
for item in mylist:
	#do something cool here...
```

But since this is so common, python provides a function for us called range.  To produce the equivalent of mylist for the for loop, 

```python
for item in range(1,5):
	#do something cool here..
```

### range function

```xml
range(<start>, <end>, [step=1])
```

The range function will return a list (ie, iterator), of values beginning with the "start"  integer value, and ending with the value preceding "end" integer value(ie, the range will
not include the "end" value).  Values between "start" and "end" are "step" values apart.  The default value for "step" is 1.  Step can be negative or positive integer values.

Here's an example that prints out the numbers from 1 to 4 (step=1 is implied):

```python
for i in range(1,5):
	print(i)
```

Here is another example.  Let us say we want to calculate the sum of all the numbers between 1 and 10:

```python
sum=0
for i in range (1, 11):
	sum=sum+i

print("The sum of numbes 1 to 10 is {sum}")
```

Let us produce all odd numbers between 1 and 10:
```python
for value in range(1, 10, 2):
	print(value)
```

produces:

```python
1
3
5
7
9
```

For loops are great if you know how many times a piece of code should execute before you start execuiting that specific piece of code.
But how do we execute code, if we don't know how many times we want to execute a piece of code?  This is where **while loops** come into play.

### While Loops

```xml
while <true condition>:
	<while body>
```

![while](images/while-loops-in-Python-300x300.webp)

Here's a classic example where we want the user to input an upper case Y or N:

```python
ans = input("Please type in Y or N: ")
while ans not in ("Y", "N"):
	print(f"Error '{ans}' is not Y or N")
	ans = input("Please type in Y or N: ")

# if we get here, ans is Y or N
```

### For  vs While

When should you write a for loop and when should you write a while loop?  It is recommended that you write a for loop, if your program knows just before
you enter the loop, how many times it needs to execute the code.  Write a while loop, if you are not sure how many times you will need to execute the loop.
For user input, it is almost always going to be a while loop, because we don't know if the user will enter in something valid the first time, or if it will
take several times.  Use a for loop, if you need to count, or if you are processing something like a list or some other data structure.

#### Additional Exercises
1. You need to write some code to ask the user to enter in a number.  Keep asking until the user enters in a valid number.   For or While loop?
2. You are writing a loop to examine each element of a list.  For or While loop?
3. Write a loop to output all the primes from 1 to 100.  For or While?
4. You ask the user to enter in Yes or No. Prompt again, if user enters in an invalid result.  For or While?
5. Look through a list of until you find an item that starts with the letter 'a'.  For or While?

6. Write a program that asks the user for the dimensions of a rectangle (20 x 20) max size.  Then print out the rectangle.  So for example, if someone
enters in 4 for the width, and 6 for the length, you would print out:

```python
Please enter the width(max: 20):  4
Please enter the length(max:20): 6

******
******
******
******
```

7. Write a program to output a multiplication table that goes from 1 to 10 as shown below.  At first don't worry about the spacing between the
numbers on a row.  Just get the numbers correct.  Go the extra mile and see if you can modify the output to get it to print the numbers in nice columns.

![10 x 10 Multiplication Table](images/mult_table_10x10.png)

8. Write a program that asks the user for a list of numbers and calculate the median, maximum, minimum and average of the numbers. To terminate
input have the user enter a period (.) when they are done entering numbers.

9. Write a loop that implements the Collatz sequence.  An example of the collatz sequence is: [12, 6, 3, 10, 5, 16, 8, 4, 2, 1]

The collatz sequence follows these rules:

a.  Begin with a positive, nozero integer called n.   (ie, ask the user for a positive, non zero integer)
b. If n is 1, the sequence is complete and you can terminate the loop
c. If n is even, the next value of n is n/2
d.  If n is odd, the next value of n is 3n + 1.

10. Given a list of numbers, write the code necessary to sum up all the positive numbers that are contained in the list.

1. Given a list of numbers, write the code necessary to sum up all the even numbers that are contained in the list.
