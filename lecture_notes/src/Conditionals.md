## Conditionals

Conditionals are expressions that evaluate to True or False.   True or False is what we call Booleans.  The data type in Python is called a bool.  
To make expressions we have operators we can use:

|Operator |Example| Explanation |
|-------|-----|-----------------|
|==|a == b	| a and b are equivalent (ie, contains the same value) |
|!=|a != b	| A is not equivalent (ie, does not contain the same value) |
|>|a > b	| a is greater than b |
|<|a < b	| a is less than b |
|>=|a >= b	| a is greater than or equal to b |
|<=|a <= b |	a is less than or equal to b |

Let us look at some examples:
```python
>>> 4 == 4
True

>>> 4 == 2 + 2
True

>>> 4 != 4
False

>>> 4 > 3
True
```

We have to be careful when we perform operations on strings and numbers.  We briefly discussed this in the string lectures, when we tried to add an integer 
and a string together:

```python
>>> 1 + "1"
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for +: 'int' and 'str'
```

Strings and numbers are not eqivalent, which can be 
```python
>>> "3" == 3
False
```

When comparing strings and numbers, == and != will work, but most of the other operators (such as > and <) will produce an error when comparing
strings and numbers.

```python
>>> "3" == 3
False

>>> "a" > 3   (produces an error, how do you compare an integer and a string?).
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: '<' not supported between instances of 'str' and 'int'
```

### Logical Operators
Logical Operators allows us to create complex expressions.
and
or
not

#### And Logic Operator

And Syntax:
```xml
  <expression 1> and <expression 2>
```

|A	| B |	A and B |
|--|--|------------|
|False| False|False|
|False| True|False|
|True|False|False|
|True|True|True|

Note: Both operands must be true for the result to be true

Example 1: Let us say that a student must have 65 credits, and must not owe the college any money to a college to apply for graduation.
credits = 65
balance = 0
credits >= 65 and balance <= 0

Example 2: To pass a class you must have a grade of 65% or better, and you must have taken the midterm exam as well as the final exam.  You can assume if someone took the midterm/final, their score is greater than 0.
grade = 70
midterm = 60
final = 80
grade >= 65 and midterm > 0 and final > 0

Example 3: To run for congress, the constitution states that a person should be at least 25 years of age and a US citizen.  Lets write a logical expression that tests this:
age = 25
UScitizen = "Y"
age >= 25 and UScitizen == "Y"

Example 4: To run for US president, the constitution states that a person must be at least 35 years of age, and a natural born citizen
NaturalBorn='Y'
age >= 35 and NaturalBorn == "Y"

 
#### Or Logic Operator

OR Syntax:
```xml
  <expression 1> or <expression 2>
```

|A|B|	A or B|
|--|--|--------|
|False	|False	|False|
|False	|True	|True|
|True	|False	|True|
|True	|True	|True|

Note: Just one operand must be true for the result to be true

Example 1: You carry an umbrella on days that have at least 50% chance of rain, or 50% chance of snow.
```python
rain = 50
snow = 10
rain >= 50 or snow >= 50
```

Example 2:  A new premium streaming service requires that you have Netflix or Hulu to join at a 10% discount.
```python
Netflix = "Y"
Hulu = "N"
Netflix == "Y" or Hulu == "N" 
```

Example 3:  The little Rascals, He-man woman haters club requires that a person be male and be between the ages of 3 and 10.
```python
gender = 'M'
age = 7
gender == 'M' and (age <= 10 or age >= 3)
```

Did you get the answer you expected?  
How about if you change the age variable to 17 and try again?
```python
gender = 'M'
age = 17
gender == 'M' and (age <=10 or age>=3)
````
I get True and we know that a person of 17 cannot be in the He-man woman haters club.  Where did we go wrong?

By the way, this is an actual bug I introduced and found later while editing my notes! 
How can we fix this so we get the right answer?

Let us first find out where things go wrong..
```python
>>> gender == 'M'
True
```
so that looks like it is correct..

How about the stuff after the first AND operand?
```python
>>> age <=10 or age>=3
True
````
O boy! that is not right.  This should evaluate to False, becuase someone who is 17 years of age is too long to be in the He-man woman haters club.  How can we fix it?
```python
>>> age=17
>>> age<=10
False

>>> age >= 3
True
```
So those seem okay, but when we OR them together we get True, but we need our result to be False.  Do we have an operator that evaluates to False when we
combine a False and True?  How about an AND operator?

```python
>>> age <=10 and age >= 3
False
```

So our expression should be:

```python
>>> gender == 'M' and age <=10 and age >=3
False
```

While testing code, it is important to try a few different values for variables just to verify that your logic is correct.  I failed to do this when I first made these notes and
I made a mistake.  These types of mistake are called logic errors.  



#### Not Logic Operator

Not syntax:
```xml
   not <expression>
```

|A|	not A|
|--|---------|
|False	|True|
|True	|False|

This just takes a Boolean expression and returns the opposite value.  Any of the expressions we have just talked about can return the opposite value by prefixing the expression with a NOT.

First find the value of expression (True or False), and then take the opposite value.
not (Netflix == "Y" or Hulu == "N")

#### Logic Operator Order of precedence?  
| Order | Operator |
| ------- | ------------ |
|1| not|
|2| and|
|3| or|

perform not operations first, then and operators, and then or.

not Netflix == "Y" or Hulu == "N"
is equivalent to:
(not Netflix == "Y") or Hulu == "N"

I personally will use parenthesis when mixing not, and, or's in expressions so that it is implied what order the operations should be executed.  Using parenthesis is not required though.

## Additional Exercises

1. Write a condition that checks if a temperature is less than 20.
2. You have two variables x and y.  Write a condition where x is greater than 20 and y is less than 20.  Give an example (values for x and y) where
this condition is true.  Give an example where this condition is false.
3.  Jimmy wears short sleeves when it is sunny outside or the temperature is above 70 degrees.  Write a condition that satifies this statement.
4. You are taking a Pass/Fail class.  You must make 75% or more on all of the homework assignments and above a 65% on the midterm or the final.
Write a condition that satifies this statement.  Assume you have three variables,  HW, midterm, and final, which represent the percentage of each.