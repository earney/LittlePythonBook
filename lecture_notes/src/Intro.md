## Introduction to Python

### What is Python?

Python is a general purpose programming language.  Which means that it was not created for one purpose only, but was created to solve problems from
many different areas.

Python is a very popular language because it is easy to learn, and easy to use.  Python is taught in many schools all around the world.

### Basic History of Python

Python was created around 1990 by Guido van Rossum, Python 2 was released in 2000, and Python 3 was released in 2008.


### Why you should learn python.

Some reasons why you should learn Python

#### 1. Easier to read
We usually write code once, but we may read it dozens of times during its lifetime.

#### 2. Portable
Most python code that runs on one operating system can run on other systems with little to no modifications. ( Windows -> Linux, etc).

#### 3. High-Level 
We don't have to focus on low level things like what OS we are running on, or how to allocate memory, etc.  Python takes care of those details for you.

#### 4. Dynamically Typed
Python is dynamically-typed. This means that the type for a value is decided at runtime, not in advance. We don’t need to specify the type of data 
while declaring it.  For example, to create a variable we just need to write something like:
a=4

#### 5. Free and Open-Source
Python is freely available. You can download it from the [Python Official Website](python.org).
Secondly, it is open-source. This means that its source code is available to the public. You can download it, read it, change it, use it, and distribute it.
This is called FLOSS(Free/Libre and Open Source Software).
As the Python community, we’re all headed toward one goal- an ever-bettering Python.
 
#### 6. Object-Oriented
A programming language that can model the real world is said to be object-oriented. It focuses on objects and combines data and functions.
Contrarily, a procedure-oriented language revolves around functions, which are code that can be reused.
Python supports both procedure-oriented and object-oriented programming which is one of the key python features.
It also supports multiple inheritance, unlike Java.
A class is a blueprint for such an object. It is an abstract data type and holds no values.

#### 7. Extensible
If needed, you can write some of your Python code in other languages like C++.
This makes Python an extensible language, meaning that it can be extended to other languages.

#### 8. Large Standard Library
Python downloads with a large library that you can use so you don’t have to write your own code for every single thing. The exhaustive libraries can be found at: https://pypi.org/

#### 9. One of the most popular programming languages:
[https://www.tiobe.com/tiobe-index/](https://www.tiobe.com/tiobe-index/)