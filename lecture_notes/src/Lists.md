## Lists

Sometimes we want to perform some action on a list of items. Python provides a list data type that is formated like so:

```xml
[ <item 1>, <item2>, ..., <item n>]

```
Below is an example of a list of some colors:

```python

colors = ["white", "black", "pink", "purple"]
```

Lists can contain any data type (values).  For example, here's a list of numbers:

```python
primes = [1, 3, 5, 7, 9, 11, 13]
```

A list can also contain the same value more than once:

```python
fibs = [0,1, 1, 2, 3, 5, 8, 13, 21]
```

Notice that fibs contains the value of 1 more than once.


A list does not necessilary need to contain all of the same types of values so the statement below is valid:

```python
mylist= [1, 3.14, "abc", "3"]
```


### Basic List Operators.

For lists to be useful, we want to be able to access items in the list.

Syntax:

```xml
  <list name>[<index number>]
```
where:
  list name is the name of the list
  index number is a integer value. 

Below is a list that contains the days of the week:

```python
weekdays = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"]
```

Python is a C based language (zero based), which means that when we access elements instead of starting with 1 we start with 0.  The first item
in the list is accessed with an index of 0.

```python
## access the first element of the list (sunday)
print(weekdays[0])   #sunday

print(weekdays[1])  #monday

print(weekdays[6]) #saturday
print(weekdays[-1]) # saturday too!
```
Python is somewhat special because we can access elements from the end of the list by using negative numbers!
-1 accesses the last element of the list, -2 accesses the second to last element of the list, and so on.

Be aware of the size of your list so that you only try to access elements in the list.  If you try to access a position in the list that does not
exist you get an index error!

```python
scores = [50, 75, 100]
print(scores[3])
```
produces

```python
Traceback (most recent call last):
  File "<stdin>", line 2, in <module>
IndexError: list index out of range
```


### Slicing a List (Taking a sublist from the list)

Sometimes we want to create a list from another list.  From the weekdays list, lets create a workday list:

syntax:
```xml
<list name> [<start index>:<end index>]
```
The new list will contain all elements starting with the start index, and ending with the element right before the end index.

```python
workdays = weekdays[1:6]
```

If you want all days except sunday, we can do the following:

```python
>>>weekdays[1:7]
or
>>>weekdays[1:]
```

if we want all days but saturday, we can do the following:

```python
>>> weekdays[0:6]
or
>>>weekdays[:6]
```
will produce a list of all element except for saturday.


### Len

```xml
len(<list name>)
```

len returns an integer containing the size of the list.  If the list is empty, len with return 0.

How to make an empty list?

```python
>>>mylist= []
>>>print(len(mylist))
0
```


There are are some operators we can use to see if a list contains values:

### In Operator

```python
>>> 3 in [1, 2, 3]
True

>>> 4 in ["abc", "123"]
False
```

### not in

Not in just produces the opposite result (True/False) of what the **in** operator:

```python
>>> "a" not in ["a", "b", "c"]
False

>>> 4 not in ["abc", "123"]
True
```

### Sort

Having a list of items in "alphabetical order" makes it more efficient to find items in a list.  If we know the list is sorted, we can stop searching
when we reach an item that is larger than the item we are searching for.  If the list is not sorted, we have to look at every item in the list.

We put the list in "alphabetical order" by sorting the list.

Python allows us to sort a list easily:

```python
>>> mylist = [100, 75, 25, 50]
>>> mylist.sort()
>>> print(mylist)

[25, 50, 75, 100]
```

### Reverse

Sometimes we want to reverse the order of the items in the list:

```python
>>>  mylist = [100, 75, 25, 50]
>>> mylist.reverse()
>>> mylist
[50, 25, 75, 100]
```

## Additional Exercises
1.  Create a list containing a shopping list of items you need to pick up at the store.  Preform the following tasks on your shopping list.
 a, Use the len function to find the number of items in the shopping list.
 b. Sort the list.  
 c. Think of an item in the list.  Use the "in" operator to verify that the item is in your list.
 d. Remove an item from the shopping list 
 e. Now verify that the item is not in the shopping list by using the "not in" operator.
 
 2.  You have a list of test scores 90, 50, 60, 80, 70.  Find the median test score.  Hint: The median is the value in the middle of a sorted list of values.
 
 3.  Write code to merge two sorted lists (In computer science we call this a merge sort): [10, 15, 20, 25], [1, 5, 12, 18]  => [1, 5, 10, 12, 15, 20, 25]
 
 
 
 
 
 