## Basic Input / Output (IO)

Communication between a user and a computer program allows a program to be more flexible, and solve a larger set of programs, then if
the data we are operating on is hardcoded in the program. Python allows a few basic ways to complete this task.
To display information to a computer screen, python provides the print function.  To 
receive information, python provides the input function which receives, by default information from the keyboard.

### Displaying text to the screen

Print "Basic" Syntax:
```xml
print(args)
```

We display text to the screen in python by using the print statement:

```python
>>> print("Hello World")
Hello World
```

You can use single quote(') or double quotes(") to designate a text string.  In the example above, Hello World is a text string, that is prefixed with a single quote (') and ends also with a single quote (').

```python
>>> print('put some text here')
put some text here
```

Please note that a text string must start and end with the same type of quote.  A text string that starts with a single quote, must end with a single quote.  A text string that starts with a double quote ("), must end with a double quote (").
You can also output other things besides strings.  For example to output a number you can do:

```python
print(1)
```
or
```python
a=1
print(a)
```

You can also combine different data types (strings, integers, floats, etc):
```python
>>>age=25
>>>print("Your age is: ", age)
```
We can use the string operations we learned during the string lecture as well:
```python
>>>print("Your age is %s" % age)
>>>print(f"Your age is {age}")
```

### Receiving input from the user (ie, keyboard)
Input Syntax:
```xml
<string variable> = input([string])
```

How to get input from the user:
```python
>>> greeting = input("Type in a greeting message. (Hello!)")
```
The greeting variable will contain whatever we typed on the keyboard.  Input stops accepting keystrokes when the user presses the enter key.
A string is returned. 

We can print out what the greeting variable contains this way:

```python
>>> print(greeting)
Hello!
```

The text 'Hello!' should be replaced with whatever you typed in. 

One thing that needs to be pointed out.  The value returned from the input function statement is always a string.  If we want to do arithmetic on input from the user, we must convert the string to a number.

For example:

```python
>>>number = Input("Enter a number and we will find it's square: ")
>>>print(number * number)
```
We get an error!  We must convert number to an integer or float first!
```python
>>>number = Input("Enter a number and we will find it's square: ")
>>>number = float(number)
>>>print(number * number)
```

Since we just asked for a number (and not a whole number), it is best to use float().
To convert the value to an integer:
```python
number = int(number)
```

To convert the value to a float (real):
```python
number = float(number)
```

### Comments

When writing programs, we usually like to add comments which helps the programmer understand what is going on, and the reasoning behind her programming.  
To create a comment, we use the # symbol.  Everything following the # symbol is ignored. If created correctly, python will just ignore the comments.

```python
# this is a comment
print("hello")   # this is a comment too!
```

### In class programming exercise:

Ask the user for the length and width of a rectangle, and output the area of the rectangle (area = length * width).

```python
#get input from user
length = input("Please enter the length of a rectangle: ")
width = input("Please enter the width of a rectangle:  ")

#convert strings to floats so we can do some math
length = float(length)
width = float(width)
Area = length * width

#print out the result
print("The area of the rectangle is : ", area)
```

What happens if you leave out the lines that convert length and width to a float?
We get the following error:
Traceback (most recent call last):

```python
  File "<stdin>", line 1, in <module>
TypeError: can't multiply sequence by non-int of type 'str'
```

That is just a fancy way of saying we cannot multiply strings.
Now try putting in some values for length and width that do not make sense, in other words, values that are not numbers, like "abc", "boo", or "12xyz"
 and see what happens. Do you get an error?  If so, what type of error?
For now, we will play nice, and only type in input that float() and int() can handle without giving errors.

### Additional Exercise:

1.  Write a program to calculate the tip that should be left after a night out at dinner.  Ask the user for the amount of the meal, as well as the 
percentage for a tip.  Then calculate and display the amount of the tip, as well as the total amount (which includes the tip).

2. Write a program to calculate a person BMI (Body Mass Index).  Ask the user for their weight in pounds, and their height in inches. Then print out
the BMI value.

BMI formula:
```xml
BMI = weight/(height * height)
```


3. Create an madlib in python by using print and input statements.   You will need to ask the user for each of the bold words in the text below: 
Also add a comment at the top of the program explaining this is a madlib program.

Use the following text:

##### Signing of the Declaration of Independence

The Declaration of Independence is a **\_\_adjective\_\_** document because it meant 
the **\_\_plural noun\_\_** of the **\_\_number\_\_** colonies decided to be
**\_\_adjective\_\_** from **\_\_country\_\_**.  In the year **\_\_number\_\_** the 
**\_\_adjective\_\_** Congress decided to **\_\_verb\_\_**  **\_\_number\_\_** leaders
 to write the Declaration of **\_\_adjective\_\_**.  They agreed to have **\_\_first name\_\_* 
 Jefferson **\_\_verb\_\_** the document and **\_\_verb\_\_** it to 
Congress after that.  After much debate, the leaders of each **\_\_noun\_\_** 
**\_\_verb\_\_** the Declaration of **\_\_adjective\_\_** on July **\_\_number\_\_**, 1776.

#### Python Documentation:

Print statement: [https://docs.python.org/3/library/functions.html#print](https://docs.python.org/3/library/functions.html#print)

Input statement: [https://docs.python.org/3/library/functions.html#input](https://docs.python.org/3/library/functions.html#input)

Built-in Functions: [https://docs.python.org/3/library/functions.html](https://docs.python.org/3/library/functions.html)