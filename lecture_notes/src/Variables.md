## Variables

Variables are containers that hold a value.  Variables allow our programs to be more flexible than just hardcoding values in our programs.

| 3 + 4	|VS| a + 4|
|---------|----|-------|
| 3 + 4 are hardcoded, their values never change	| VS |	a can be any value that we give it. |
|  | | This is much more flexible.|

 In the table above, a is a variable.  The variable a can any number we want it to be.  It can be assigned a value from the user or some other source.
To keep things simple, a variable name should start with an upper or lower case letter.  A variable name should not start with a number (0 – 9) or 
a arithmetic operator (+,-,*,/,%, etc). Variable names should also describe the data it will hold.

Here are some valid variable names:

* total
* sum
* balance
* name
* fname
* lname
* age
* date
* width
* length

Some invalid variable names are:

| Variable name | Reason |
| ------------------- |----------- |
|19name | A variable name cannot start with a number | 
| -abc | A variable name cannot start with a non alphabetic letter |
|first-name| A variable cannot contain the minus sign ||

Also remember that variables should describe the data they hold, so something like myvariable, would not be a good name.  Neither is single 
letter variable names like a, b, c,  x, y, z.  There are cases where single letter variable names are acceptable, but in most cases it is best to use a 
variable name that describes the data the variable contains.

Let us create a variable named sum and assign it a value of 7:

```python
>>> sum=7
```

typing the name of the variable (and pressing enter) will return the value stored in that variable:

```python
>>> sum
7
```
You can add a value to a variable:

```python
>>> sum=sum+5
```

There is a short hand for this expression:
```python
>>> sum+=5
```

So to be clear,  sum=sum+5 is the same as writing sum+=5.


A variable can hold a value of any length, the only limitation being the amount of memory available.

```python
>>> total=9999999999999999999999999999999999999
>>> total
9999999999999999999999999999999999999
```

It is good practice to use an underscore (_) to parse a number if you have very large numbers, so that the number is easier to read:

```python
>>> balance = 10_000_000
>>> balance
10000000
```

We can also put floats (reals) into a variable:

```python
>>> length=1.12
>>> length
1.12
```

We can also do math with variables:

```python
>>> length = 3
>>> length * 4
12
```
	or
```python
>>> segment1 = 3.5
>>> segment2 = 2
>>> segment1 + segment2
5.5
```

One things to point out..  You can redefine a variable after it has been defined.  You can even define it as a different type:

```python
>>> a = 4
>>> a = 3.14
a
3.14
```

This is perfectly legal in python.  Many older languages will not allow you to assign the variable a different type.

You can assign variables any value you want.
For example:

```python
>>> a = 4
….  (sometime later in the same session)
>>> a = 3.14
….  (sometime later in the same session)
>>> a = "my string"
```

Python is fine with this.  It assumes the programmer knows what they are doing and allows the assignments to different types.  Redefining variables as different types is a bad programming practice, so please do it sparingly!!   There are times this make sense to do, but 99% of the time it is not a good idea, and will cause issues as you write larger programs.

Let us define a variable named pi that holds the value 3.14159

```python
>>> pi = 3.14159
```

Let us use our definition of pi to calculate the area of a circle (Area = pi * r2) with a radius of 2:

```python
>>> pi=3.14159
>>> r = 2
>>> pi * r * r 
12.56636
```

### Additional Exercises:

1. Use the formula Area = length * width to calculate the area of a rectangle.  Create variables length and width, and assign each of them a positive number.  Then calculate the area of the rectangle.  Try different values for length and width and check your results to see that the calculations are performed correctly.

2. You are in a foreign country that uses Celsius for their temperature readings.  You ask someone for the current temperature, and they say it is 15 degrees Celsius.  Write a program that assigns 15 to a variable named Celsius and calculate the degrees in Fahrenheit using the following formula:

	Fahrenheit = 9/5 * Celsius + 32

	Some other values to use for Celsius: 0, 100

3.	Create a new variable and assign it the value of 50.  Now try to assign the same variable a value of 70.  Does this work?  Why or why not?